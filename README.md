# Serverless SaaS Demo Application

This repository contains the code for a Serverless SaaS (Software as a Service) demo application built using AWS CDK (Cloud Development Kit) and TypeScript. The application demonstrates a multi-tenant architecture with user authentication using Amazon Cognito, serverless backend APIs deployed with AWS Lambda and API Gateway, and a React.js frontend.

## Application Overview

The Serverless SaaS Demo consists of the following components:

1. **Cognito Stage**: Deploys all resources required for multi-tenant Identity Provider (IDP) configuration using Amazon Cognito.
1. **Infra Stage**: Deploys the VPC (Virtual Private Cloud) and networking resources required for AWS Lambda functions.
1. **Service Stage**: Deploys API Gateway and Lambda functions for the backend API. It interacts with the Cognito user pool for authentication.
1. **Client Stage**: Deploys the React.js client-side web app. The client app is connected to the API Gateway and allows users to fetch and update tenant-specific data based on the IDP JWT (JSON Web Token).

## Deployment Instructions

Follow these steps to deploy the Serverless SaaS Demo application using AWS CDK:

### Prerequisites

1. **AWS CLI**: Ensure that you have the AWS CLI installed and configured with the necessary credentials.
1. **AWS CDK CLI**: Install and bootstrap the AWS CDK CLI to interact with the Cloud Development Kit.
1. **Node.js and npm**: Install Node.js and npm on your machine.
1. **Create AWS CodePipeline GitLab Connection:**
    * Go to the [AWS CodePipeline Connections page](https://eu-central-1.console.aws.amazon.com/codesuite/settings/connections).
    * Create a new connection for GitLab repository and **make note of the connection ARN**.

### Steps

1. **Clone the Repository:**
```
git clone https://gitlab.com/saasarch/serverless-saas-demo-cdk.git
cd serverless-saas-demo-cdk
```
2. **Install Dependencies:**
```
npm ci
```
3. **Build the CDK App:**
```
npm run build
```
4. **Deploy the CDK Pipeline:**
```
cdk deploy ServerlessSaaSDemoPipelineStack --parameters connectionArn=${CODEPIPELINE_CONNECTION_ARN}
```
5. **Monitor the Pipeline:**
Once deployed, monitor the AWS CodePipeline in the AWS Management Console. It will go through the Cognito, Infra, Service, and Client stages.
6. **Access the Application:**
After the pipeline completes, you can access the deployed React.js web app. The URL will be displayed in the ClientStage-ClientStack stack in outputs.

## Cleanup

To remove the deployed resources and clean up the environment, first empty AWS S3 bucket that contains React.js frontend. The bucket name should be similar to `clientstage-clientstack-serverlesssaasdemoclientbu-1234567890ab`

Then delete those Cloudformation stacks one after another:

1. ClientStage-ClientStack
1. ServiceStage-ApiStack
1. InfraStage-PoolLambdaStack
1. InfraStage-VpcStack
1. CognitoStage-CognitoStack

Then run:

```
cdk destroy ServerlessSaaSDemoPipelineStack
```

This will delete all the stacks created by the CDK pipeline.