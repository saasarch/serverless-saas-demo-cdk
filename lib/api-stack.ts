import { Construct } from 'constructs';
import { CfnOutput, Stack, StackProps } from 'aws-cdk-lib';
import { Function } from 'aws-cdk-lib/aws-lambda';
import { LambdaRestApi, CognitoUserPoolsAuthorizer } from 'aws-cdk-lib/aws-apigateway';
import Config from './config';
import { LambdaIntegrationNoPermission } from './lambda-integration-no-perm';
import { ServicePrincipal } from 'aws-cdk-lib/aws-iam';
import { UserPool } from 'aws-cdk-lib/aws-cognito';

interface ApiStackProps extends StackProps {
    userPoolId: string
}

export class ApiStack extends Stack {
    public readonly apiBaseUrl: CfnOutput

    constructor(scope: Construct, id: string, props: ApiStackProps) {
        super(scope, id, props);

        const handler = Function.fromFunctionName(this, 'SaasDemoApiGwLambda', Config.poolLambdaName)

        const stageName = 'Demo'
        const api = new LambdaRestApi(this, 'SaasDemoApiGw', {
            deploy: true,
            deployOptions: {
                stageName,
                tracingEnabled: true,
            },
            proxy: false,
            handler,
            defaultCorsPreflightOptions: {
                allowHeaders: [
                    'Content-Type',
                    'X-Amz-Date',
                    'Authorization',
                    'X-Api-Key',
                ],
                allowMethods: ['OPTIONS', 'GET', 'POST'],
                allowCredentials: true,
                allowOrigins: [`*`],
            },
        })

        this.apiBaseUrl = new CfnOutput(
            this,
            'SaasDemoApiGwUrl',
            {
                exportName: `SaasDemoApiGwUrl`,
                value: `https://${api.restApiId}.execute-api.${process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION}.amazonaws.com/${stageName}`
            }
        )

        const userPool = UserPool.fromUserPoolId(this, 'SaasDemoApiGwAuthorizerUserPool', props.userPoolId)

        const authorizer = new CognitoUserPoolsAuthorizer(this, 'SaasDemoApiGwAuthorizer', {
            cognitoUserPools: [userPool],
        });

        const integration = new LambdaIntegrationNoPermission(handler, { proxy: true, allowTestInvoke: true })

        const tenantData = api.root.addResource('tenant-data')
        tenantData.addMethod(
            'GET',
            integration,
            {
                authorizer,
            }
        )
        tenantData.addMethod(
            'POST',
            integration,
            {
                authorizer,
            }
        )

        handler.addPermission('SaasDemoApiGwLambdaPermission', {
            principal: new ServicePrincipal('apigateway.amazonaws.com'),
            sourceArn: api.arnForExecuteApi("*"),
        })
    }
}
