import { Construct } from 'constructs';
import { FunctionProps, Runtime, Function, Code } from 'aws-cdk-lib/aws-lambda';
import { Duration, Stack, StackProps } from 'aws-cdk-lib';
import { Vpc } from 'aws-cdk-lib/aws-ec2';
import { ManagedPolicy, Role, ServicePrincipal } from 'aws-cdk-lib/aws-iam';
import Config from './config';

export interface PoolLambdaStackProps extends StackProps {
    vpc: Vpc,
    identityPoolId: string
    userPoolUrl: string
}

export class PoolLambdaStack extends Stack {

    constructor(scope: Construct, id: string, props: PoolLambdaStackProps) {
        super(scope, id, props);

        const roleName = `SaaSPoolLambdaRole`

        const role = new Role(this, roleName, {
            assumedBy: new ServicePrincipal('lambda.amazonaws.com'),
            roleName,
        })
        role.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaVPCAccessExecutionRole'));

        const functionName = Config.poolLambdaName

        const lambdaProps: FunctionProps = {
            code: Code.fromAsset('sources/serverless-saas-demo-pool-lambda.zip'),
            retryAttempts: 0,
            vpc: props.vpc,
            handler: 'dist/index.handler',
            runtime: Runtime.NODEJS_20_X,
            functionName,
            role,
            memorySize: 256,
            timeout: Duration.seconds(3),
            environment: {
                IDENTITY_POOL_ID: props.identityPoolId, 
                USER_POOL_URL: props.userPoolUrl,
            },
        }

        new Function(this, 'ServerlessSaasDemoPoolLambda', lambdaProps)
    }
}
