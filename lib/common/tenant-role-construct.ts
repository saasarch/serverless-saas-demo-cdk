import { Construct } from 'constructs'
import { CfnRole, Effect, PolicyDocument, PolicyStatement } from 'aws-cdk-lib/aws-iam'

export interface TenantRoleConstructProps {
    name: string
    authRoleArn: string
}

export class TenantRoleConstruct extends Construct {
    public readonly role: CfnRole

    constructor(scope: Construct, id: string, props: TenantRoleConstructProps) {
        super(scope, id)

        const policyDocument = new PolicyDocument({
            statements: [new PolicyStatement({
                effect: Effect.ALLOW,
                actions: [
                    "dynamodb:GetItem",
                    "dynamodb:PutItem",
                ],
                resources: [`arn:aws:dynamodb:${process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION}:${process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT}:table/SaaSTenantTable${props.name}`],
            })]
        })

        const roleName = `SaaSTenantRole${props.name}`
        const policyName = `SaaSTenantPolicy${props.name}`

        this.role = new CfnRole(this, roleName, {
            assumeRolePolicyDocument: {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Principal": {
                            "AWS": props.authRoleArn
                        },
                        "Action": [
                            "sts:AssumeRoleWithWebIdentity",
                            "sts:AssumeRole",
                        ]
                    }
                ]
            },
            policies: [{
                policyDocument,
                policyName,
            }],
            roleName,
        })
    }
}