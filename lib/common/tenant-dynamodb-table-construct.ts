import { Construct } from 'constructs'
import { AttributeType, BillingMode, Table } from 'aws-cdk-lib/aws-dynamodb'
import { RemovalPolicy } from 'aws-cdk-lib'
import { AwsCustomResource, AwsCustomResourcePolicy, PhysicalResourceId } from 'aws-cdk-lib/custom-resources'

export interface TenantDynamoDBTableConstructProps {
    name: string
}

export class TenantDynamoDBTableConstruct extends Construct {

    constructor(scope: Construct, id: string, props: TenantDynamoDBTableConstructProps) {
        super(scope, id)

        const tableName = `SaaSTenantTable${props.name}`
        const table = new Table(this, tableName, {
            tableName,
            partitionKey: {
                name: 'TenantStorageType',
                type: AttributeType.STRING,
            },
            removalPolicy: RemovalPolicy.DESTROY,
            billingMode: BillingMode.PAY_PER_REQUEST,
        });

        new AwsCustomResource(this, `SaaSTenantTableInitTable${props.name}`, {
            onCreate: {
                service: 'DynamoDB',
                action: 'putItem',
                parameters: {
                    TableName: table.tableName,
                    Item: {
                        TenantStorageType: { S: 'tenantData' },
                        TenantData: { S: props.name },
                    }
                },
                physicalResourceId: PhysicalResourceId.of(table.tableName + '_initialization')
            },
            policy: AwsCustomResourcePolicy.fromSdkCalls({ resources: AwsCustomResourcePolicy.ANY_RESOURCE }),
        });
    }
}