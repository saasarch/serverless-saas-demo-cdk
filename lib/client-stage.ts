import { CfnOutput, Stage, StageProps } from 'aws-cdk-lib'
import { Construct } from 'constructs'
import { ClientStack } from './client-stack'

export class ClientStage extends Stage {
    public readonly bucketName: CfnOutput

    constructor(scope: Construct, id: string, props?: StageProps) {
        super(scope, id, props)

        const clientStack = new ClientStack(this, 'ClientStack', {
            env: {
                account: process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT,
                region: process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION
            }
        })

        this.bucketName = clientStack.bucketName
    }
}