import { CfnOutput, Stage, StageProps } from 'aws-cdk-lib'
import { Construct } from 'constructs'
import { ApiStack } from './api-stack'

interface ServiceStageProps extends StageProps {
    userPoolId: string
}

export class ServiceStage extends Stage {
    public readonly apiBaseUrl: CfnOutput

    constructor(scope: Construct, id: string, props: ServiceStageProps) {
        super(scope, id, props)

        const apiStack = new ApiStack(this, 'ApiStack', {
            env: {
                account: process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT,
                region: process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION
            },
            userPoolId: props.userPoolId,
        })

        this.apiBaseUrl = apiStack.apiBaseUrl
    }
}