import { CfnOutput, RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib';
import { Distribution, OriginAccessIdentity } from 'aws-cdk-lib/aws-cloudfront';
import { S3Origin } from 'aws-cdk-lib/aws-cloudfront-origins';
import { Bucket, BucketAccessControl } from 'aws-cdk-lib/aws-s3';
import { Construct } from 'constructs';

export class ClientStack extends Stack {
    public readonly bucketName: CfnOutput

    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        const bucket = new Bucket(this, 'ServerlessSaaSDemoClientBucket', {
            accessControl: BucketAccessControl.PRIVATE,
            removalPolicy: RemovalPolicy.DESTROY,
        })

        this.bucketName = new CfnOutput(
            this,
            'ServerlessSaaSDemoClientBucketName',
            {
                exportName: `ServerlessSaaSDemoClientBucketName`,
                value: bucket.bucketName,
            }
        )

        const originAccessIdentity = new OriginAccessIdentity(this, 'OriginAccessIdentity');
        bucket.grantRead(originAccessIdentity);

        const dist = new Distribution(this, 'ServerlessSaaSDemoClientDistribution', {
            defaultRootObject: 'index.html',
            defaultBehavior: {
                origin: new S3Origin(bucket, { originAccessIdentity }),
            },
        })

        new CfnOutput(
            this,
            'ServerlessSaaSDemoClientDistributionUrl',
            {
                exportName: `ServerlessSaaSDemoClientDistributionUrl`,
                value: `https://${dist.distributionDomainName}`
            }
        )
    }
}
