import { Stage, StageProps } from 'aws-cdk-lib'
import { Construct } from 'constructs'
import { VpcStack } from './vpc-stack'
import { PoolLambdaStack } from './pool-lambda-stack'

export interface InfraStageProps extends StageProps {
    identityPoolId: string
    userPoolUrl: string
}

export class InfraStage extends Stage {

    constructor(scope: Construct, id: string, props: InfraStageProps) {
        super(scope, id, props)

        const vpcStack = new VpcStack(this, 'VpcStack', {
            env: {
                account: process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT,
                region: process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION
            }
        })

        new PoolLambdaStack(this, 'PoolLambdaStack', {
            env: {
                account: process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT,
                region: process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION
            },
            vpc: vpcStack.vpc,
            identityPoolId: props.identityPoolId,
            userPoolUrl: props.userPoolUrl,
        })
    }
}