import { Construct } from 'constructs'
import { CfnRole, Effect, PolicyDocument, PolicyStatement } from 'aws-cdk-lib/aws-iam'

export interface AuthRoleConstructProps {
    identityPoolId: string
}

export class AuthRoleConstruct extends Construct {
    public readonly roleArn: string

    constructor(scope: Construct, id: string, props: AuthRoleConstructProps) {
        super(scope, id)

        const policyDocument = new PolicyDocument({
            statements: [new PolicyStatement({
                effect: Effect.ALLOW,
                actions: [
                    "cognito-identity:GetCredentialsForIdentity"
                ],
                resources: [`*`],
            }), new PolicyStatement({
                effect: Effect.ALLOW,
                actions: [
                    "sts:AssumeRole",
                    "sts:AssumeRoleWithWebIdentity"
                ],
                resources: [`arn:aws:iam::${process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT}:role/SaaSTenantRole*`],
            })]
        })

        const roleName = `SaaSAuthenticatedRole`
        const policyName = `SaaSAuthenticatedRolePolicy`

        const role = new CfnRole(this, roleName, {
            assumeRolePolicyDocument: {
                "Version": "2012-10-17",
                "Statement": [
                    {
                        "Effect": "Allow",
                        "Principal": {
                            "Federated": "cognito-identity.amazonaws.com"
                        },
                        "Action": "sts:AssumeRoleWithWebIdentity",
                        "Condition": {
                            "StringEquals": {
                                "cognito-identity.amazonaws.com:aud": props.identityPoolId,
                            },
                            "ForAnyValue:StringLike": {
                                "cognito-identity.amazonaws.com:amr": "authenticated"
                            }
                        }
                    }
                ]
            },
            policies: [{
                policyDocument,
                policyName,
            }],
            roleName,
        })
        
        this.roleArn = role.attrArn
    }
}