interface IConfig {
    cidr: string
    poolLambdaName: string
}

const Config: IConfig = {
    cidr: process.env.SAAS_DEMO_VPC_CIDR ?? '10.128.0.0/16',
    poolLambdaName: 'serverless-saas-demo-pool-lambda'
}

export default Config