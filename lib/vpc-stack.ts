import { Construct } from 'constructs';
import Config from './config';
import { CfnEIP, CfnNatGateway, CfnRoute, IpAddresses, SubnetType, Vpc } from 'aws-cdk-lib/aws-ec2';
import { Stack, StackProps } from 'aws-cdk-lib';

export class VpcStack extends Stack {
    public readonly vpc: Vpc

    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        this.vpc = new Vpc(this, 'SaasVpc', {
            ipAddresses: IpAddresses.cidr(Config.cidr),
            natGateways: 0,
            maxAzs: 2,
            enableDnsHostnames: true,
            enableDnsSupport: true,
            subnetConfiguration: [
                {
                    cidrMask: 25,
                    name: 'public',
                    subnetType: SubnetType.PUBLIC,
                },
                {
                    cidrMask: 20,
                    name: 'application',
                    subnetType: SubnetType.PRIVATE_WITH_EGRESS,
                },
            ],
        })

        const publicSubnetA = this.vpc.publicSubnets[0];
        const eip = new CfnEIP(this, 'NatGwAEIP')
        const nat = new CfnNatGateway(this, "NatGwA", {
            allocationId: eip.attrAllocationId,
            subnetId: publicSubnetA.subnetId
        });

        this.vpc.privateSubnets.forEach(({ routeTable: { routeTableId } }, index) => {
            new CfnRoute(this, 'PrivateSubnetRoute' + index, {
                destinationCidrBlock: '0.0.0.0/0',
                routeTableId,
                natGatewayId: nat.attrNatGatewayId,
            })
        })

    }
}
