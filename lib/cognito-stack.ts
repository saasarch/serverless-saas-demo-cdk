import { Construct } from 'constructs';
import { CfnIdentityPool, CfnIdentityPoolRoleAttachment, CfnUserPoolGroup, CfnUserPoolUser, CfnUserPoolUserToGroupAttachment, UserPool } from 'aws-cdk-lib/aws-cognito';
import { tenants } from './types';
import { TenantRoleConstruct } from './common/tenant-role-construct';
import { AuthRoleConstruct } from './auth-role-construct';
import { AwsCustomResource, AwsCustomResourcePolicy, PhysicalResourceId } from 'aws-cdk-lib/custom-resources';
import { CfnOutput, RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib';
import { TenantDynamoDBTableConstruct } from './common/tenant-dynamodb-table-construct';

export class CognitoStack extends Stack {
    public readonly identityPoolId: CfnOutput
    public readonly userPoolUrl: CfnOutput
    public readonly userPoolId: CfnOutput
    public readonly userPoolClientId: CfnOutput

    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props);

        const pool = new UserPool(this, 'SaaSDemoUserPool', {
            removalPolicy: RemovalPolicy.DESTROY,
        });
        const client = pool.addClient('serverless-saas-demo-app', {
            oAuth: {
                flows: {
                    authorizationCodeGrant: true,
                },
                callbackUrls: ['https://localhost:3000/callback'],
                logoutUrls: ['https://localhost:3000/logout'],
            },
        });

        this.userPoolUrl = new CfnOutput(
            this,
            'SaaSDemoUserPoolUrl',
            {
                exportName: `SaaSDemoUserPoolUrl`,
                value: `cognito-idp.${process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION}.amazonaws.com/${pool.userPoolId}`,
            }
        )

        this.userPoolId = new CfnOutput(
            this,
            'SaaSDemoUserPoolId',
            {
                exportName: `SaaSDemoUserPoolId`,
                value: pool.userPoolId,
            }
        )

        this.userPoolClientId = new CfnOutput(
            this,
            'SaaSDemoUserPoolClientId',
            {
                exportName: `SaaSDemoUserPoolClientId`,
                value: client.userPoolClientId,
            }
        )

        const identityPool = new CfnIdentityPool(this, 'SaaSDemoIdentityPool', {
            allowUnauthenticatedIdentities: false,
            cognitoIdentityProviders: [{
                clientId: client.userPoolClientId,
                providerName: pool.userPoolProviderName,
            }]
        });

        this.identityPoolId = new CfnOutput(
            this,
            'SaaSDemoIdentityPoolId',
            {
                exportName: `SaaSDemoIdentityPoolId`,
                value: identityPool.ref,
            }
        )

        const authRoleConstruct = new AuthRoleConstruct(this, 'AuthRoleConstruct', {
            identityPoolId: identityPool.ref
        })

        new CfnIdentityPoolRoleAttachment(this, 'SaaSDemoIdentityPoolRoleAttachment', {
            identityPoolId: identityPool.ref,
            roles: {
                'authenticated': authRoleConstruct.roleArn
            }
        });

        for (const tenant of tenants) {
            const role = new TenantRoleConstruct(this, `TenantRoleConstruct${tenant}`, {
                name: tenant,
                authRoleArn: authRoleConstruct.roleArn,
            })
            const groupName = tenant
            const group = new CfnUserPoolGroup(this, `SaaSDemoUserPoolGroup${tenant}`, {
                userPoolId: pool.userPoolId,
                groupName,
                roleArn: role.role.attrArn,
            });
            group.node.addDependency(role)
            const username = tenant.toLowerCase()
            const email = `${username}@example.com`
            const user = new CfnUserPoolUser(this, `SaaSDemoUserPoolGroupUser${tenant}`, {
                userPoolId: pool.userPoolId,
                username,
                userAttributes: [
                    { name: "email", value: email },
                    { name: "email_verified", value: "true" },
                ],
            });
            user.node.addDependency(group)
            const res = new AwsCustomResource(this, `SetTenantUserPassword${tenant}`, {
                onCreate: {
                    service: "CognitoIdentityServiceProvider",
                    action: "adminSetUserPassword",
                    parameters: {
                        UserPoolId: pool.userPoolId,
                        Username: username,
                        Password: '$3creTP@$$worD',
                        Permanent: true,
                    },
                    physicalResourceId: PhysicalResourceId.of("SetTestUserPassword"),
                },
                policy: AwsCustomResourcePolicy.fromSdkCalls({
                    resources: AwsCustomResourcePolicy.ANY_RESOURCE,
                }),
            })
            res.node.addDependency(user)
            const attachment = new CfnUserPoolUserToGroupAttachment(this, `SaaSDemoUserPoolGroupUserToGroupAttachment${tenant}`, {
                groupName,
                username,
                userPoolId: pool.userPoolId,
            });
            attachment.node.addDependency(user)

            new TenantDynamoDBTableConstruct(this, `TenantDynamoDBTableConstruct${tenant}`, {
                name: tenant,
            })
        }
    }
}
