import { Construct } from 'constructs'
import { CodePipeline, CodePipelineSource, ShellStep } from 'aws-cdk-lib/pipelines'
import { Effect, PolicyStatement } from 'aws-cdk-lib/aws-iam'
import { CognitoStage } from './cognito-stage'
import { InfraStage } from './infra-stage'
import { ServiceStage } from './service-stage'
import { CfnParameter, Stack, StackProps, pipelines } from 'aws-cdk-lib'
import { ClientStage } from './client-stage'

export class ServerlessSaaSDemoPipelineStack extends Stack {

    constructor(scope: Construct, id: string, props?: StackProps) {
        super(scope, id, props)

        const commands = [
            'npm ci',
            'npm i -g aws-cdk',
            'npm run build',
            'cdk synth --no-rollback',
        ]

        const connectionArn = new CfnParameter(this, "connectionArn", {
            type: "String",
            description: "The connection ARN for gitlab repository."
        });

        const pipeline = new CodePipeline(this, 'ServerlessSaaSDemoPipelinenPipeline', {
            pipelineName: 'ServerlessSaaSDemoPipeline',
            codeBuildDefaults: this.codeBuildDefaults,
            synth: new ShellStep('Synth', {
                commands,
                input: CodePipelineSource.connection(
                    'saasarch/serverless-saas-demo-cdk',
                    'main',
                    {
                        connectionArn: connectionArn.valueAsString,
                    }
                )
            }),
        })

        this.createStages(pipeline)

        pipeline.buildPipeline()

    }

    private createStages(pipeline: pipelines.CodePipeline) {

        const cognitoStage = new CognitoStage(this, 'CognitoStage')
        pipeline.addStage(cognitoStage)
        pipeline.addStage(new InfraStage(this, 'InfraStage', {
            identityPoolId: cognitoStage.identityPoolId.importValue,
            userPoolUrl: cognitoStage.userPoolUrl.importValue,
        }))
        const serviceStage = new ServiceStage(this, 'ServiceStage', {
            userPoolId: cognitoStage.userPoolId.importValue,
        })
        pipeline.addStage(serviceStage)

        const clientStage = new ClientStage(this, 'ClientStage')
        const clientStageDeployment = pipeline.addStage(clientStage)

        const clientBuildShellStep = new ShellStep('ServerlessSaaSDemoClientBuildShellStep', {
            envFromCfnOutputs: {
                REACT_APP_SERVER_BASE_URL: serviceStage.apiBaseUrl,
                REACT_APP_USER_POOL_ID: cognitoStage.userPoolId,
                REACT_APP_USER_POOL_APP_CLIENT_ID: cognitoStage.userPoolClientId,
                AWS_S3_BUCKET: clientStage.bucketName,
            },
            commands: [
                [
                    'git clone https://gitlab.com/saasarch/serverless-saas-demo-client.git',
                    'cd serverless-saas-demo-client',
                    'npm i',
                    'npm run build',
                    'cd build',
                    'aws s3 sync . s3://${AWS_S3_BUCKET}/',
                ].join(' && ')
            ],
        })
        clientStageDeployment.addPost(clientBuildShellStep)
    }

    private codeBuildDefaults = {
        rolePolicy: [new PolicyStatement({
            effect: Effect.ALLOW,
            actions: [
                'codeartifact:GetRepositoryEndpoint',
                'codeartifact:GetDomainPermissionsPolicy',
                'codeartifact:ReadFromRepository',
                'codeartifact:PublishToRepository',
                'codeartifact:GetAuthorizationToken',
            ],
            resources: [`arn:aws:codeartifact:*`],
        }), new PolicyStatement({
            effect: Effect.ALLOW,
            actions: [
                'sts:GetServiceBearerToken',
            ],
            resources: [`*`],
        }), new PolicyStatement({
            effect: Effect.ALLOW,
            actions: [
                's3:List*',
                's3:CopyObject',
                's3:GetObject',
                's3:PutObject',
            ],
            resources: [
                `arn:aws:s3:::clientstage-clientstack-*`,
                `arn:aws:s3:::clientstage-clientstack-*/*`,
            ],
        }), new PolicyStatement({
            effect: Effect.ALLOW,
            actions: [
                'ec2:DescribeAvailabilityZones',
            ],
            resources: [`*`],
        })]
    }
}