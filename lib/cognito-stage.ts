import { CfnOutput, Stage, StageProps } from 'aws-cdk-lib'
import { Construct } from 'constructs'
import { CognitoStack } from './cognito-stack'

export class CognitoStage extends Stage {
    public readonly identityPoolId: CfnOutput
    public readonly userPoolUrl: CfnOutput
    public readonly userPoolId: CfnOutput
    public readonly userPoolClientId: CfnOutput

    constructor(scope: Construct, id: string, props?: StageProps) {
        super(scope, id, props)

        const cognitoStack = new CognitoStack(this, 'CognitoStack', {
            env: {
                account: process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT,
                region: process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION
            }
        })

        this.identityPoolId = cognitoStack.identityPoolId
        this.userPoolUrl = cognitoStack.userPoolUrl
        this.userPoolId = cognitoStack.userPoolId
        this.userPoolClientId = cognitoStack.userPoolClientId
    }
}