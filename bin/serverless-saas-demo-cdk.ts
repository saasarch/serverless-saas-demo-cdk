#!/usr/bin/env node
import 'source-map-support/register';
import { ServerlessSaaSDemoPipelineStack } from '../lib/pipeline';
import { App } from 'aws-cdk-lib';

const app = new App();
new ServerlessSaaSDemoPipelineStack(app, 'ServerlessSaaSDemoPipelineStack', {
  env: {
    account: process.env.CDK_DEPLOY_ACCOUNT || process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEPLOY_REGION || process.env.CDK_DEFAULT_REGION
  }
});